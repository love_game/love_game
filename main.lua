-- a Lua Love program for learning

-- load local modules
require('funcs')
require('menu')
require('scene')


math.randomseed(love.timer.getTime())
love.math.setRandomSeed(love.timer.getTime())

-- global variables
scale = {x=2.2,y=2.2}
paused = false
fullscreen = false
showFPS = false
states = {}
sw = 1280
sh = 720

function love.keypressed(key)
    
    -- handle global function keys

    if key == 'f11' then
        fullscreen = not fullscreen
        love.window.setFullscreen(fullscreen) -- disabled for now
    elseif key == 'f5' then
        showFPS = not showFPS
    else
        --~ print('Unknown key')
    end
    
    -- handle state function keys
    states[current].keypressed(key)
    
end


function love.load()

    love.window.setMode(sw,sh,{fullscreen=fullscreen,vsync=false})
    love.window.setTitle('LOVE game')
    love.mouse.setVisible(false)
    os = love.system.getOS()
    cpucount = love.system.getProcessorCount()
    print('====================================================')
    print('System Info')
    print('====================================================')
    print(' OS:\t' .. os)
    print(' CPUs:\t' .. cpucount)
    
    -- load sprites into lib
    SpriteLib = {}
    SpriteLib.size = 0
    load_sprites()
    print(SpriteLib.size)
    
    -- add states to table
    states.menu = menu
    states.scene = scene
    current = 'menu'
    
    states[current].load()
    
    -- do misc. stuff
    
    
end




function love.draw()

    -- draw current state
    states[current].draw()

    -- print some stats
    if( showFPS ) then
        local fps = love.timer.getFPS()
        love.graphics.print(fps .. ' fps',0,20)
        love.graphics.print(last_dt .. ' sec')
    end
end



function love.update(dt)

    states[current].update(dt)
    last_dt = dt

end


function love.quit()
    -- exit program
    states[current].quit()
    print('shutting down program ...')
end


require 'sprite'




sti = require 'libs.sti'
scene = {}




function scene.load()
    -- load scene
    scene.map = sti.new('map/simple')
    scene.scale = scale
    scene.x = 0
    scene.y = 0

    love.graphics.setBackgroundColor(0,20,45)
    
    local props = scene.map.properties
    for k,v in pairs(props) do
        print(k .. '\t' .. v)
    end
    
    local ts = scene.map.tilesets
    for k,v in pairs(ts) do
        print(k..'\t'..v.name)
        local t = v.tiles
        for k,v in pairs(t) do
            print('\t'..k..'\t'..v.id)
        end
    end
    
    local scrnWidth = love.window.getWidth()
    local scrnHeight = love.window.getHeight()
    
    -- sprite list
    scene.spriteList = {}
    for i=1,20 do
        local x = math.random() * scrnWidth
        local y = math.random() * scrnHeight
        local pos = {x=x,y=y}
        scene.spriteList[i] = Sprite:new('rock.png',pos)
    end
end


function scene.keypressed(key)
    -- handle keypresses
    if key == 'escape' then
        switch_state('menu')
    end    
end


function scene.update(dt)
    -- update scene state
    for _,v in pairs(scene.spriteList) do
        v:update(dt)
    end
    
    -- movement keys
    if love.keyboard.isDown('w') then
        scene.y = scene.y + 200*dt
    end
    if love.keyboard.isDown('s') then
        scene.y = scene.y - 200*dt
    end
    if love.keyboard.isDown('a') then
        scene.x = scene.x + 200*dt
    end
    if love.keyboard.isDown('d') then
        scene.x = scene.x - 200*dt
    end
    
    if love.keyboard.isDown('-') then
        local reps = math.floor(dt*1000)
        for i=1,reps do
            if #scene.spriteList then
                table.remove(scene.spriteList)
            end
        end
    elseif love.keyboard.isDown('=') then
        local reps = math.floor(dt*1000)
        for i=1,reps do
            local pos = {x=math.random()*love.window.getWidth(),y=math.random()*love.window.getHeight()}
            local sprite = Sprite('rock.png',pos)
            table.insert(scene.spriteList,sprite)
        end
    end
    
    
    scene.map:setDrawRange(scene.x,scene.y,sw,sh)
    scene.map:update(dt)
end


function scene.draw()
    -- draw scene
    love.graphics.push()
    
    love.graphics.scale(scene.scale.x,scene.scale.y)
    love.graphics.translate(scene.x,scene.y)
    
    -- draw the map
    scene.map:draw()
    
    love.graphics.pop()
    
    for _,v in pairs(scene.spriteList) do
        v:draw()
    end
    
    love.graphics.print('x: ' .. -scene.x .. '\ny: ' .. -scene.y,0,40)
    love.graphics.print('sprites: ' .. #scene.spriteList,0,80)
end


function scene.quit()
    print('quitting scene ...')
end



-- oscillator for sound sample generation
function Oscillator(freq)
    local phase = 0
    return function()
        phase = phase + 2*math.pi/rate            
        if phase == 2*math.pi then
            phase = phase - 2*math.pi
        end 
        return math.sin(freq*phase)
    end
end



-- run the quit() and load() functions of the old and new state
function switch_state(new_state)
    local old = current
    states[current].quit()
    current = new_state
    states[current].load()
    print('switching state: ' .. old .. ' --> ' .. new_state)
end



-- load an image into/from the global image library
function get_sprite(img)
    -- check for presence in library
    if not SpriteLib[img] then
        SpriteLib[img] = love.graphics.newImage('sprite/'..img)
        SpriteLib.size = SpriteLib.size + 1
    end
    -- return image
    return SpriteLib[img]
end


function load_sprites()
    local d = love.filesystem.getDirectoryItems('sprite/')
    for _,v in pairs(d) do
        get_sprite(v)
    end
end

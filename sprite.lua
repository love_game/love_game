

require('class')
require('funcs')

Sprite = class()



function Sprite:construct(img,pos)
    self.img = get_sprite(img)
    self.pos = pos
end



function Sprite:new(img,pos)
    local tmpSprite = Sprite(img,pos)
    return tmpSprite
end


function Sprite:print()
    print('image: '..self.img)
    print('pos: '..self.pos.x..' '..self.pos.y)
end


function Sprite:draw()
    love.graphics.draw(self.img,self.pos.x,self.pos.y)
end


function Sprite:update(dt)
    pos = self.pos
    pos.x = pos.x + dt * math.random(-1,1) * 100
    pos.y = pos.y + dt * math.random(-1,1) * 100
end
